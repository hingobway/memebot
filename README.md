# memebot
This bot spams memes.

## Installation

1. Install [node.js](https://nodejs.org/en/download)
2. Extract the zip you got into a new folder
3. Shift+Right-click inside the folder and click "open command window here"
4. In terminal, type `npm install` and then hit enter. Wait for it to finish, and don't close the window afterwards.
6. In discord, press Ctrl+Shift+I. Then click "Application" at the top, then on the side click Local Storage, and https://disco.... and then at the bottom you should see "token". Right-click the token, and click "Edit Value". Then copy the whole thing.
7. Open token.js, and paste the token where it says. Make sure there is only one set of quotation marks around the token.
7. Back in terminal, type `node .` and press enter. Wait until you see "ready."

## Use

8. In any server or DM, to start the spamming, type `**GIMME A MEME**` to start the process.
9. To stop, send `time to stop` as a part of any message.
