console.log('prepping...');
const disc = require('discord.js');
const fs=require('fs');
const token=require('./token.js');

var bot=new disc.Client();
var loop;
var count=0;

bot.on('ready',()=>{
  console.log('Ready.');
});

bot.on('message',(message)=>{
  if(message.author.id==bot.user.id){
    if(message.content=='**GIMME A MEME**'){
      loop=setInterval(()=>{
        message.channel.send(fs.readFileSync('text.txt','utf-8'))
        .then(()=>{
          count++;
          console.log(`Sent a meme. (#${count})`);
        })
        .catch((error)=>{
          console.log(error);
        });
      },1000);
    }else if(message.content.match(/time to stop/i)){
      clearInterval(loop);
      console.log(`Stopped memeing. Sent ${count} memes.`);
      count=0;
    }
  }
});

bot.login(token);
